//Import thư viện ExpressJS
// import express from 'express';
const express = require('express');

// Khởi tạo app nodejs bằng express
const app = express();

// Khai báo ứng dụng sẽ chạy trên cổng 8000
const port = 8000;

// Khai báo ứng dụng đọc được body raw json
app.use(express.json());

// Chạy ứng dụng
app.listen(port, () => {
    console.log("Ứng dụng chạy trên cổng: ", port);
})
class Drink {
    id;
    maNuocUong;
    tenNuocUong;
    donGia;
    ngayTao;
    ngayCapNhat;
    constructor(paramId,paramMaNuoc,paramTenNuoc,paramDonGia){
        this.id = paramId;
        this.maNuocUong = paramMaNuoc;
        this.tenNuocUong = paramTenNuoc;
        this.donGia = paramDonGia;
        this.ngayTao = "14/7/2021";
        this.ngayCapNhat = "14/7/2021";
    }
}
var drinkListObj = [];
var drink1 = new Drink(1, "TRATAC", "Trà tắc", 10000);
var drink2 = new Drink(2, "COCA", "Cocacola", 15000);
var drink3 = new Drink(3, "PEPSI", "Pepsi", 15000);
drinkListObj.push(drink1);
drinkListObj.push(drink2);
drinkListObj.push(drink3);
console.log(drinkListObj);

//tạo api lấy request query
app.get("/drinks-class", (request, response) => {
    var query = request.query;
    return response.json(drinkListObj);
})

//tạo api lấy request query
app.get("/drinks-object", (request, response) => {
    var query = request.query;
    return response.json(drinkListObj);
})

